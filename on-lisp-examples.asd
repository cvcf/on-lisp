(defsystem :on-lisp-examples
  :description "Example code from Paul Graham's book, On Lisp"
  :version "1.0.0"
  :maintainer "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :serial t
  :pathname "examples/"
  :components ((:module "networks"
                :components ((:file "package")
                             (:file "twenty-questions")))
               (:module "bezier"
                :components ((:file "package")
                             (:file "bezier-curves")))
               (:module "reader"
                :components ((:file "package")
                             (:file "reader"))))
  :depends-on (:on-lisp))
