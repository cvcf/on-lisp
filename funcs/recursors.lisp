(in-package #:onlisp)

(defun lrec (rec &optional base)
  (labels ((self (lst)
             (if (null lst)
                 (if (functionp base)
                     (funcall base)
                     base)
                 (funcall rec
                          (first lst)
                          (lambda () (self (rest lst)))))))
    #'self))

(defun trec (rec &optional (base #'identity))
  (labels ((self (tree)
             (if (atom tree)
                 (if (functionp base)
                     (funcall base tree)
                     base)
                 (funcall rec
                          tree
                          (lambda () (self (first tree)))
                          (lambda () (and (rest tree) (self (rest tree))))))))
    #'self))

(defun ttrav (rec &optional (base #'identity))
  (labels ((self (tree)
             (if (atom tree)
                 (if (functionp base)
                     (funcall base tree)
                     base)
                 (funcall rec
                          (self (first tree))
                          (and (rest tree) (self (rest tree)))))))
    #'self))
