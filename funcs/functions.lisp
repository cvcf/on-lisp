(in-package #:onlisp)

(defvar *!equivs* (make-hash-table))

(defun ! (fn)
  (or (gethash fn *!equivs*) fn))

(defun def! (fn fn!)
  (setf (gethash fn *!equivs*) fn!))

(defun memoize (fn)
  (let ((cache (make-hash-table :test #'equal)))
    (lambda (&rest args)
      (multiple-value-bind (val win) (gethash args cache)
        (if win
            val
            (setf (gethash args cache) (apply fn args)))))))

(defun compose (&rest fns)
  (if fns
      (let ((fn1 (last1 fns))
            (fns (butlast fns)))
        (lambda (&rest args)
          (reduce #'funcall fns
                  :from-end t
                  :initial-value (apply fn1 args))))
      #'identity))

(defun fif (if-fn then-fn &optional else-fn)
  (lambda (x)
    (if (funcall if-fn x)
        (funcall then-fn x)
        (funcall else-fn x))))

(defun fintersection (fn &rest fns)
  (if fns
      (let ((chain (apply #'fintersection fns)))
        (lambda (x)
          (and (funcall fn x)
               (funcall chain x))))
      fn))

(defun funion (fn &rest fns)
  (if fns
      (let ((chain (apply #'funion fns)))
        (lambda (x)
          (or (funcall fn x)
              (funcall chain x))))
      fn))
