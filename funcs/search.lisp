(in-package #:onlisp)

(defun find* (fn lst)
  (if lst
      (let ((val (funcall fn (first lst))))
        (if val
            (values (first lst) val)
            (find* fn (rest lst))))))

(defun before (x y lst &key (test #'eql))
  (and lst
       (let ((item (first lst)))
         (cond ((funcall test y item) nil)
               ((funcall test x item) lst)
               (t (before x y (rest lst) :test test))))))

(defun after (x y lst &key (test #'eql))
  (let ((rest (before y x lst :test test)))
    (and rest (member x rest :test test))))

(defun duplicate (x lst &key (test #'eql))
  (member x (rest (member x lst :test test))
          :test test))

(defun split-if (fn lst)
  (let ((acc nil))
    (do ((src lst (rest src)))
        ((or (null src) (funcall fn (first lst)))
         (values (nreverse acc) src))
      (push (first src) acc))))

(defun most (fn lst)
  (if lst
      (let* ((wins (first lst))
             (max  (funcall fn wins)))
        (dolist (x (rest lst))
          (let ((score (funcall fn x)))
            (when (> score max)
              (setq wins x
                    max  score))))
        (values wins max))
      (values nil nil)))

(defun most* (fn lst)
  (if lst
      (let ((res (list (first lst)))
            (max (funcall fn (first lst))))
        (dolist (x (rest lst))
          (let ((score (funcall fn x)))
            (cond ((> score max) (setq max score
                                       res (list x)))
                  ((= score max) (push x res)))))
        (values (nreverse res) max))
      (values nil nil)))

(defun best (fn lst)
  (if lst
      (let ((wins (first lst)))
        (dolist (x (rest lst))
          (if (funcall fn x wins)
              (setq wins x)))
        wins)))
