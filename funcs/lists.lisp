(in-package #:onlisp)

(proclaim '(inline last1 singleton-p append1 conc1 mklist))

(defun last1 (lst)
  (first (last lst)))

(defun singleton-p (lst)
  (and (consp lst) (null (rest lst))))

(defun append1 (lst item)
  (append lst (list item)))

(defun conc1 (lst item)
  (nconc lst (list item)))

(defun mklist (item)
  (if (listp item) item (list item)))

(defun longer (x y)
  (labels ((compare (x y)
             (and (consp x)
                  (or (null (first y))
                      (compare (rest x) (rest y))))))
    (if (and (listp x) (listp y))
        (compare x y)
        (> (length x) (length y)))))

(defun map-filter (fn lst)
  (let ((acc nil))
    (dolist (item lst (nreverse acc))
      (let ((val (funcall fn item)))
        (if val (push val acc))))))

(defun group (source n)
  (if (zerop n) (error "zero length!"))
  (labels ((rec (source acc)
             (let ((rest (nthcdr n source)))
               (if (consp rest)
                   (rec rest (cons (subseq source 0 n) acc))
                   (nreverse (cons source acc))))))
    (if source
        (rec source nil))))

(defun flatten (tree)
  (labels ((rec (tree acc)
             (cond ((null tree) acc)
                   ((atom tree) (cons tree acc))
                   (t (rec (first tree)
                           (rec (rest tree) acc))))))
    (rec tree nil)))

(defun prune (fn tree)
  (labels ((rec (tree acc)
             (cond ((null tree)          (nreverse acc))
                   ((consp (first tree)) (rec (rest tree)
                                              (cons (rec (first tree) nil) acc)))
                   (t (rec (rest tree)
                           (if (funcall fn (first tree))
                               acc
                               (cons (first tree) acc)))))))
    (rec tree nil)))
