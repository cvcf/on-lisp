(defsystem :on-lisp
  :description "Implementation of functions and macros from Paul Graham's book, On Lisp"
  :version "1.0.0"
  :maintainer "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :serial t
  :components ((:file "package")
               (:module "funcs"
                :components ((:file "lists")
                             (:file "search")
                             (:file "mapping")
                             (:file "io")
                             (:file "symbols")
                             (:file "functions")
                             (:file "recursors")))
               (:module "macros"
                :components ((:file "tiny")
                             (:file "context")
                             (:file "iteration")
                             (:file "generalized-variables")
                             (:file "comparisons")
                             (:file "anaphoric")
                             (:file "functions")
                             (:file "lazy")
                             (:file "reader")
                             (:file "destructuring")))))
