(defpackage #:ol-examples/bezier
  (:use #:cl #:ol)
  (:nicknames #:bz)
  (:export #:*points*
           #:gen-bezier))
