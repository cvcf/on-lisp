(in-package #:20qv1)

(defstruct node contents yes no)

(defparameter *nodes* nil)

(let ((*nodes* (make-hash-table)))
  (defun defnode (name conts &optional yes no)
    (setf (gethash name *nodes*)
          (make-node :contents conts :yes yes :no no)))

  (defun run-node (name)
    (let ((n (gethash name *nodes*)))
      (cond ((node-yes n)
             (format t "~a~%>> " (node-contents n))
             (case (readkw)
               (:yes (run-node (node-yes n)))
               (:no  (run-node (node-no  n)))))
            (t (node-contents n))))))

(in-package #:20qv2)

(defparameter *nodes* nil)

(let ((*nodes* (make-hash-table)))
  (defun defnode (name conts &optional yes no)
    (setf (gethash name *nodes*)
          (if yes
              (lambda ()
                (format t "~a~%>> " conts)
                (case (readkw)
                  (:yes (funcall (gethash yes *nodes*)))
                  (:no  (funcall (gethash no  *nodes*)))))
              (lambda () conts))))

  (defun run-node (name)
    (funcall (gethash name *nodes*))))

(in-package #:20qv3)

(defparameter *nodes* nil)

(let ((*nodes* nil))
  (defun defnode (&rest args)
    (push args *nodes*)
    args)

  (defun compile-network (root)
    (let ((node (assoc root *nodes*)))
      (when node
        (let ((conts (second node))
              (yes   (third node))
              (no    (fourth node)))
          (if yes
              (let ((yes-fn (compile-network yes))
                    (no-fn  (compile-network no)))
                (lambda ()
                  (format t "~a~%>> " conts)
                  (funcall (if (eq (readkw) :yes) yes-fn no-fn))))
              (lambda () conts))))))

  (defun run-node (name)
    (funcall (compile-network name))))

(in-package #:20q)

;;;;
;; An example of the first version of the network from fig. 6.3 on pg. 78
(defun define-nodes (pkg)
  (let ((fn (reread pkg ":defnode")))
    (funcall fn :people  "Is the person a man?" :male    :female)
    (funcall fn :male    "Is he living?"        :liveman :deadman)
    (funcall fn :deadman "Was he American?"     :us      :them)
    (funcall fn :us      "Is he on a coin?"     :coin    :cidence)
    (funcall fn :coin    "Is the coin a penny?" :penny   :coins)
    (funcall fn :penny :lincoln)))

(defun run (&optional (version 'v3))
  (let ((pkg (mksym '20q version)))
    (define-nodes pkg)
    (funcall (reread pkg ":run-node") :people)))
