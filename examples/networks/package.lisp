(defpackage #:ol-examples/twenty-questions/v1
  (:use #:cl #:ol)
  (:nicknames #:20qv1 #:20q/v1)
  (:export #:defnode
           #:run-node))

(defpackage #:ol-examples/twenty-questions/v2
  (:use #:cl #:ol)
  (:nicknames #:20qv2 #:20q/v2)
  (:export #:defnode
           #:run-node))

(defpackage #:ol-examples/twenty-questions/v3
  (:use #:cl #:ol)
  (:nicknames #:20qv3 #:20q/v3)
  (:export #:defnode
           #:run-node))

(defpackage #:ol-examples/twenty-questions
  (:use #:cl #:ol)
  (:nicknames #:20q)
  (:export #:run))
