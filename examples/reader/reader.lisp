(in-package #:reader)

(defdelim #\[ #\] (x y)
  `'(,@(mapx-y #'identity (ceiling x) (floor y))))

(defdelim #\{ #\} (&rest args)
  `(fn (compose ,@args)))
