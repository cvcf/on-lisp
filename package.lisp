(defpackage #:onlisp
  (:use #:cl)
  (:nicknames #:pgl #:ol)
  (:export
   ;;;; funcs
   ;; lists.lisp
   #:last1
   #:singleton-p
   #:append1
   #:conc1
   #:mklist
   #:longer
   #:map-filter
   #:group
   #:flatten
   #:prune

   ;; search.lisp
   #:find*
   #:before
   #:after
   #:duplicate
   #:split-if
   #:most
   #:most*
   #:best

   ;; mapping.lisp
   #:map0-n
   #:map1-n
   #:mapx-y
   #:map->
   #:mappend
   #:mapcars
   #:rmapcar

   ;; io.lisp
   #:readlist
   #:readkw
   #:prompt
   #:break-loop

   ;; symbols.lisp
   #:mkstr
   #:mksym
   #:reread
   #:explode

   ;; functions.lisp
   #:!
   #:def!
   #:memoize
   #:compose
   #:fif
   #:fintersection
   #:funion

   ;; recursors.lisp
   #:lrec
   #:trec
   #:ttrav

   ;;;; macros
   ;; tiny.lisp
   #:mac
   #:mac1
   #:abbrev
   #:abbrevs
   #:propname
   #:propnames

   ;; context.lisp
   #:with-bind
   #:with-bind*
   #:with-gensyms
   #:condlet

   ;; evaluation.lisp
   #:if3
   #:nif
   #:in
   #:inq
   #:in-if
   #:>case

   ;; iteration.lisp
   #:while
   #:until
   #:for
   #:dotuples/open
   #:dotuples/closed
   #:mvdo
   #:mvdo*
   #:mvpsetq

   ;; generalized-variables.lisp
   #:allf
   #:nilf
   #:tf
   #:toggle
   #:concf
   #:conc1f
   #:concnew
   #:_f
   #:pull
   #:pull-if
   #:popn
   #:sortf

   ;; comparisons.lisp
   #:most-of
   #:nthmost

   ;; anaphoric.lisp
   #:if*
   #:when*
   #:while*
   #:and*
   #:cond*
   #:lambda*
   #:block*
   #:if+
   #:when+
   #:while+
   #:cond+
   #:it
   #:self
   #:a+
   #:list+
   #:defanaphoric

   ;; functions.lisp
   #:fn
   #:lrec*
   #:on-cdrs
   #:trec*
   #:on-trees

   ;; lazy.lisp
   #:delay
   #:force

   ;; reader.lisp
   #:defdelim

   ;; destructuring.lisp
   #:dbind
   #:with-array
   #:with-matrix
   #:with-struct
   #:with-places))
