(in-package #:onlisp)

(defmacro allf (val &rest args)
  (with-gensyms (gval)
    `(let ((,gval ,val))
       ;; Why MAPCAN?
       (setf ,@(mapcan (lambda (a) (list a gval)) args)))))

(defmacro nilf (&rest args)
  `(allf nil ,@args))

(defmacro tf (&rest args)
  `(allf t ,@args))

(defmacro toggle (&rest args)
  `(progn
     ,@(mapcar (lambda (arg) `(toggle*, arg)) args)))

(define-modify-macro toggle* () not)

(define-modify-macro conc1f (x) conc1)

(define-modify-macro concnew (x &rest args)
  (lambda (place x &rest args)
    (unless (apply #'member x place args)
      (nconc place (mklist x)))))

(defmacro _f (op place &rest args)
  (multiple-value-bind (vars forms var set access)
      ;; It's worth noting that PG refers to this as GET-SETF-METHOD but, from
      ;; what I can tell, that symbol doesn't actually exist and
      ;; GET-SETF-EXPANSION seems to do what we want.
      (get-setf-expansion place)
    `(let* (,@(mapcar #'list vars forms)
            (,(first var) (,op ,access ,@args)))
       ,set)))

(defmacro pull (x place &rest args)
  (multiple-value-bind (vars forms var set access) (get-setf-expansion place)
    (let ((g (gensym)))
      `(let* ((,g ,x)
              ,@(mapcar #'list vars forms)
              (,(first var) (delete ,g ,access ,@args)))
         ,set))))

(defmacro pull-if (test place &rest args)
  (multiple-value-bind (vars forms var set access) (get-setf-expansion place)
    (let ((g (gensym)))
      `(let ((,g ,test)
             ,@(mapcar #'list vars forms)
             (,(first var) (delete-if ,g ,access ,@args)))
         ,set))))

(defmacro popn (n place)
  (multiple-value-bind (vars forms var set access) (get-setf-expansion place)
    (with-gensyms (gn glst)
      `(let* ((,gn ,n)
              ,@(mapcar #'list vars forms)
              (,glst ,access)
              (,(first var) (nthcdr ,gn ,glst)))
         (prog1 (subseq ,glst 0 ,gn)
           ,set)))))

(defmacro sortf (op &rest places)
  (let* ((methods (mapcar (lambda (p) (multiple-value-list (get-setf-expansion p)))
                          places))
         (temps   (apply #'append (mapcar #'third methods))))
    `(let* ,(mapcar #'list
                    (mapcan (lambda (m) (append (first m) (third m)))
                            methods)
                    (mapcan (lambda (m) (append (second m) (list (fifth m))))
                            methods))
       ,@(mapcon (lambda (remaining)
                   (mapcar (lambda (arg)
                             `(unless (,op ,(first remaining) ,arg)
                                (rotatef ,(first remaining) ,arg)))
                           (rest remaining)))
                 temps)
       ,@(mapcar #'fourth methods))))
