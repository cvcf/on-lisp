(in-package #:onlisp)

(defmacro if* (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))

(defmacro when* (test &body body)
  `(if* ,test (progn ,@body)))

(defmacro while* (expr &body body)
  `(do ((it ,expr ,expr))
       ((not it))
     ,@body))

(defmacro and* (&rest args)
  (cond ((null args)        t)
        ((null (rest args)) (first args))
        (t                  `(if* ,(first args) (and* ,@(rest args))))))

(defmacro cond* (&rest clauses)
  (when clauses
    (let ((c (first clauses))
          (s (gensym)))
      `(let ((,s ,(first c)))
         (if ,s
             (let ((it ,s)) ,@(rest c))
             (cond* ,@(rest clauses)))))))

(defmacro lambda* (params &body body)
  `(labels ((self ,params ,@body))
     #'self))

(defmacro block* (tag &rest args)
  `(block ,tag
     ,(funcall (lambda* (args)
                 (case (length args)
                   (0 nil)
                   (1 (first args))
                   (t `(let ((it ,(first args)))
                         ,(self (rest args))))))
               args)))

(defmacro if+ (test &optional then else)
  (let ((win (gensym)))
    `(multiple-value-bind (it ,win) ,test
       (if (or it ,win) ,then ,else))))

(defmacro when+ (test &body body)
  `(if+ ,test (progn ,@body)))

(defmacro while+ (test &body body)
  (let ((flag (gensym)))
    `(let ((,flag t))
       (while ,flag
         (if+ ,test
              (progn ,@body)
              (setq ,flag nil))))))

(defmacro cond+ (&rest clauses)
  (when clauses
    (let ((c   (first clauses))
          (val (gensym))
          (win (gensym)))
      `(multiple-value-bind (,val ,win) ,(first c)
         (if (or ,val ,win)
             (let ((it ,val)) ,@(rest c))
             (cond+ ,@(rest clauses)))))))

(let ((g (gensym)))
  (defun read* (&optional (stream *standard-input*))
    (let ((val (read stream nil g)))
      (unless (equal val g)
        (values val t)))))

(defmacro do-file (filename &body body)
  (let ((s (gensym)))
    `(with-open-file (,s ,filename)
       (while+ (read* ,s)
         ,@body))))

(defmacro a+ (&rest args)
  (a+expand args nil))

(defun a+expand (args syms)
  (if args
      (let ((sym (gensym)))
        `(let* ((,sym (first args))
                (it   ,sym))
           ,(a+expand (rest args)
                      (append syms (mklist sym)))))
      `(+ ,@syms)))

(defmacro list+ (&rest args)
  (list+expand args nil))

(defun list+expand (args syms)
  (if args
      (let ((sym (gensym)))
        `(let* ((,sym (first args))
                (it   ,sym))
           ,(list+expand (rest args)
                         (append syms (mklist sym)))))
      `(list ,@syms)))

(defmacro defanaphoric (name &key calls (rule :all))
  (let* ((opname (or calls (pop-symbol name)))
         (body   (case rule
                   (:all   `(anaphoric-expr1 args '(,opname)))
                   (:first `(anaphoric-expr2 ',opname args))
                   (:place `(anaphoric-expr3 ',opname args)))))
    `(defmacro ,name (&rest args)
       ,body)))

(defun anaphoric-expr1 (args expr)
  (if args
      (let ((sym (gensym)))
        `(let* ((,sym (first args))
                (it   ,sym))
           ,(anaphoric-expr1 (rest args)
                             (append expr (mklist sym)))))
      expr))

(defun pop-symbol (sym)
  (intern (subseq (symbol-name sym) 1)))

(defun anaphoric-expr2 (op args)
  `(let ((it ,(first args)))
     (,op it ,@(rest args))))

(defun anaphoric-expr3 (op args)
  `(_f (lambda (it) (,op it ,@(rest args))) ,(first args)))
