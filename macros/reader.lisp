(in-package #:onlisp)

(defmacro defdelim (left right params &body body)
  `(ddfn ,left ,right (lambda ,params ,@body)))

(let ((rparen (get-macro-character #\))))
  (defun ddfn (left right fn)
    (set-macro-character right rparen)
    (set-dispatch-macro-character #\# left
      (lambda (stream char1 char2)
        (declare (ignorable char1 char2))
        (apply fn (read-delimited-list right stream t))))))
