(in-package #:onlisp)

(defmacro fn (expr) `#',(rbuild expr))

(defun rbuild (expr)
  (if (or (atom expr) (eq (first expr) 'lambda))
      expr
      (if (eq (first expr) 'compose)
          (build-compose (rest expr))
          (build-call (first expr) (rest expr)))))

(defun build-compose (fns)
  (let ((g (gensym)))
    `(lambda (,g)
       ,(labels ((rec (fns)
                   (if fns
                       `(,(rbuild (first fns)) ,(rec (rest fns)))
                       g)))
          (rec fns)))))

(defun build-call (op fns)
  (let ((g (gensym)))
    `(lambda (,g)
       (,op ,@(mapcar (lambda (f)
                        `(,(rbuild f) ,g))
                      fns)))))

(defmacro lrec* (rec &optional base)
  (let ((gfn (gensym)))
    `(lrec (lambda (it ,gfn)
             (symbol-macrolet ((rec (funcall ,gfn)))
               ,rec))
           ,base)))

(defmacro on-cdrs (rec base &rest lsts)
  `(funcall (lrec* ,rec (lambda () ,base)) ,@lsts))

(defmacro trec* (rec &optional (base 'it))
  (let ((lfn (gensym)) (rfn (gensym)))
    `(trec (lambda (it ,lfn ,rfn)
             (symbol-macrolet ((left (funcall ,lfn))
                               (right (funcall ,rfn)))
               ,rec))
           (lambda (it) ,base))))

(defmacro on-trees (rec base &rest trees)
  `(funcall (trec* ,rec ,base) ,@trees))
