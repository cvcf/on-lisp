(in-package #:onlisp)

(defmacro when-bind ((var expr) &body body)
  `(let ((,var ,expr))
     (when ,var
       ,@body)))

(defmacro when-bind* (binds &body body)
  (if (null binds)
      `(progn ,@body)
      `(let (,(first binds))
         (if ,(caar binds)
             (when-bind* ,(rest binds) ,@body)))))

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar (lambda (s) `(,s (gensym))) syms)
     ,@body))

(defmacro condlet (clauses &body body)
  (let ((body-fn (gensym))
        (vars    (mapcar (lambda (v) (cons v (gensym)))
                         (remove-duplicates
                          (mapcar #'first (mappend #'rest clauses))))))
    `(labels ((,body-fn ,(mapcar #'first vars)
                ,@body))
       (cond ,@(mapcar (lambda (c)
                         (condlet-clause vars c body-fn))
                       clauses)))))

(defun condlet-clause (vars clause body-fn)
  (let* ((vars* (mapcar #'rest vars))
         (decl  `(declare (ignorable ,@vars*))))
    `(,(first clause) (let ,vars*
                        ,decl
                        (let ,(condlet-binds vars clause)
                          (,body-fn ,@vars*))))))

(defun condlet-binds (vars clause)
  (mapcar (lambda (bind-form)
            (if (consp bind-form)
                (cons (rest (assoc (first bind-form) vars))
                      (rest bind-form))))
          (rest clause)))
