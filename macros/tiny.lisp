(in-package #:onlisp)

(defmacro mac (expr)
  `(pprint (macroexpand ',expr)))

(defmacro mac1 (expr)
  `(pprint (macroexpand-1 ',expr)))

(defmacro abbrev (short long)
  `(defmacro ,short (&rest args)
     `(,',long ,@args)))

(defmacro abbrevs (&rest names)
  `(progn
     ,@(mapcar (lambda (pair)
                 `(abbrev ,@pair))
               (group names 2))))

(defmacro propmacro (name)
  `(defmacro ,name (x)
     `(get ,x ',',name)))

(defmacro propmacros (&rest properties)
  `(progn
     ,@(mapcar (lambda (p)
                 `(propmacro ,p))
               properties)))
